<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bukusaldoagen extends Model
{
  protected $fillable = [
      'id','user_id','no_trx','tgl_trx','nominal','mutasi','saldo','aktif','keterangan','created_at','created_by','updated_at','updated_by','deleted_at','deleted_by'
  ];
}
